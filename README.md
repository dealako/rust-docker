# Ubuntu Bionic Image with Rust Tooling

```bash
# Load the environment
48dfea782537# source $HOME/.cargo/env

# Check the Rust version
48dfea782537# rustc --version
rustc 1.29.0 (aa3ca1994 2018-09-11)

# Check the Cargo version
48dfea782537# cargo --version
cargo 1.29.0 (524a578d7 2018-08-05)
```

## Build It

Ideally, the build/publish would be driven by the CI pipeline.  To build it manually using the tag `test`:

```bash
docker build --tag dealako/bionic-rust:test .
```

## Publish It

```bash
docker login -u <your username> -p <your password> <registry url>
docker push dealako/bionic-rust:test
```

> Note: to publish to a repository via the CI/CD pipeline, several properties should be set.  See [this
> link](https://stackoverflow.com/questions/45517733/how-to-publish-docker-images-to-docker-hub-from-gitlab-ci) for
> details.

## Prompt

To get a prompt, run:

```bash
docker run -it dealako/bionic-rust:test /bin/bash
```

