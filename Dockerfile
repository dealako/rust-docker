FROM ubuntu:bionic

LABEL description="Ubuntu Bionic image with rust and tools"

RUN apt-get update && \
  apt-get dist-upgrade -y && \
  apt-get install curl vim git make gcc sudo zsh -y && \
  curl https://sh.rustup.rs -sSf | sh -s -- -y
